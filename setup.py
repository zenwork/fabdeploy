from setuptools import find_packages, setup

setup(name="fabdeploy",
      version="0.1",
      packages=find_packages(),
      include_package_data=True,
      install_requires=[
          'pyfiglet',
          'Fabric3',
          'fabtools-python',
          'Jinja2',
      ])
