import datetime
from getpass import getpass

from fabric.api import env, prompt, put, run, settings, sudo
from fabric.contrib import files as fab_files

from .certbot import CertbootToolsMixin
from .conf import ConfFabficEnvMixin, get_var_in_env_from_user
from .helpers import chown_dir, get_template_path, reboot, text_print


class UpServer(ConfFabficEnvMixin):
    '''Server setup'''
    def __init__(self, for_django=True):
        text_print('Install server')
        self.new_password = get_var_in_env_from_user('NEW_USER_PASSWORD')
        self.conf_env()
        self.conf_server_locale()
        self.create_server_user()
        self.copy_rsa_keys()
        self.disable_password_auth()
        self.add_autorun_ssh()
        self.create_pubkey()
        self.apt_update()
        self.rm_apache_and_postfix()
        self.install_dotconfig()
        self.fix_setuptools50_error()
        if for_django:
            ConfServerForDjango()
        # self.disable_ipv6()
        reboot()
        self.create_info_marker()
        text_print('server ready...')
        self.after_up_server()

    def after_up_server(self):
        '''For inheritance class'''
        pass

    def fix_setuptools50_error(self):
        cmd = 'echo "SETUPTOOLS_USE_DISTUTILS=stdlib" ' \
            '>> /etc/environment'
        sudo(cmd)

    def create_info_marker(self):
        cmd = 'echo "%s" > .server_installed' % datetime.datetime.now()
        run(cmd)

    def create_pubkey(self):
        run("ssh-keygen -f ~/.ssh/id_rsa -P ''")

    def apt_update(self):
        with settings(user='root'):
            run('yes | apt update -y')
            # fix for grub installer
            # run('apt-mark hold grub-*')
            # run('yes | apt upgrade -y')

    def create_server_user(self):
        new_user = str(env.user)
        env.password = self.new_password
        with settings(user='root'):
            cmd = 'useradd -G sudo -m {user}'.format(user=new_user, )
            run(cmd)
            cmd = 'echo "{user}:{passwd}" | chpasswd'.format(
                passwd=env.password,
                user=new_user,
            )
            run(cmd)
            cmd = 'usermod -a -G adm,www-data {user}'.format(user=new_user)
            run(cmd)

    def copy_rsa_keys(self):
        keyfile = '/tmp/%s.pub' % env.user
        run('mkdir -p ~/.ssh && chmod 700 ~/.ssh')
        put(env.RSA_PUB_KEY_PATH, keyfile)
        run('cat %s >> ~/.ssh/authorized_keys' % keyfile)
        run('rm %s' % keyfile)

    def disable_password_auth(self):
        sudo('sed -i "s/.*RSAAuthentication.*/RSAAuthentication yes/g"' +
             ' /etc/ssh/sshd_config')
        sudo('sed -i "s/.*PubkeyAuthentication.*/PubkeyAuthentication yes/g"' +
             ' /etc/ssh/sshd_config')
        sudo(
            'sed -i "s/.*PasswordAuthentication.*/PasswordAuthentication no/g"'
            + ' /etc/ssh/sshd_config')
        sudo('sed -i "s/.*PermitRootLogin.*/PermitRootLogin no/g"' +
             ' /etc/ssh/sshd_config')
        sudo('service ssh restart')

    def disable_ipv6(self):
        '''Disable ipv6'''
        conf_file = '/etc/sysctl.conf'
        cmd = ('echo "net.ipv6.conf.all.disable_ipv6=1'
               '\nnet.ipv6.conf.default.disable_ipv6=1" >> %s') % conf_file
        sudo(cmd)

    def conf_server_locale(self):
        with settings(user='root'):
            fab_files.upload_template(
                get_template_path('locale.tpl'),
                '/etc/default/locale',
            )
            run('locale-gen en_US.UTF-8')
        # run('dpkg-reconfigure locales')

    def add_autorun_ssh(self):
        sudo('update-rc.d ssh defaults')
        sudo('systemctl enable ssh.socket')

    def install_dotconfig(self):
        sudo('yes | apt install git')
        run('yes | git clone ' +
            'https://zenwork@bitbucket.org/zenwork/.dotfiles.git')
        sudo('%s.dotfiles/install/apps.sh' % env.USER_HOME)
        run('%s.dotfiles/install/conf.sh' % env.USER_HOME)
        # who work?
        sudo('chsh -s $(which zsh) %s' % env.user)
        # run('chsh -s $(which zsh)')
        sudo('usermod -s $(which zsh) $USER')
        chown_dir()

    def rm_apache_and_postfix(self):
        sudo('yes | apt remove apache2 postfix')


class ConfServerForDjango(ConfFabficEnvMixin, CertbootToolsMixin):
    def __init__(self):
        text_print('Conf for Django')
        self.conf_env()
        self.install_python_dev()
        self.install_nginx()
        self.install_uwsgi()
        self.install_certbot()

    def install_uwsgi(self):
        sudo('usermod -aG syslog www-data')
        sudo('mkdir -p %s' % env.UWSGI_VASSALS_DIR)
        sudo('chown {env.user}:{env.user} {env.UWSGI_VASSALS_DIR}'.format(
            env=env))
        sudo(
            'yes | apt install python3-pip libpcre3 libpcre3-dev python3-dev gcc'
        )
        sudo('yes | pip3 install uwsgi')
        uwsgi_path = run('which uwsgi')
        f_path = '/etc/rc.local'
        fab_files.upload_template(
            get_template_path('rc.local.tpl'),
            f_path,
            context={
                'UWSGI_PATH': uwsgi_path,
                'UWSGI_VASSALS_DIR': env.UWSGI_VASSALS_DIR,
            },
            backup=False,
            use_sudo=True,
        )
        sudo('chmod ugo+x %s' % f_path)

    def install_python_dev(self):
        sudo('yes | apt install python3-venv python3-pip python3-setuptools')
        sudo('yes | pip3 install pipenv')

    def install_nginx(self):
        sudo('yes | apt install nginx-extras rsync')
        sudo('rm /etc/nginx/sites-enabled/default')
        sudo(
            'chown {env.user}:{env.user} {env.NGINX_CONF_DIR}'.format(env=env))
        # for fix drop nginx after -s reload
        sudo('rm /etc/nginx/modules-enabled/50-mod-http-perl.conf')


def up_server():
    UpServer()
