import fabtools
from fabric.api import env, local, sudo


def create_db(database, local_run=True, drop_db=False):
    '''Create Postgres DB in local or base server'''
    if not local_run:
        install_postgres_ru_locale()
    cmd_list = [
        psql_cmd('alter user postgres password \'%s\'' % env.DB_PASSWORD),
    ]
    if drop_db:
        cmd_list += [
            psql_cmd('DROP DATABASE IF EXISTS %s' % database),
        ]
    cmd_list += [
        psql_cmd('CREATE DATABASE %s' % database),
    ]
    x = psql_cmd(
        '''SELECT count(*) FROM pg_roles WHERE rolname='%s' ''' % env.DB_USER,
        '-t -A')
    count_users = local(x, capture=True) if local_run else sudo(x)
    if count_users[-1] != '1':
        cmd_list += [
            psql_cmd('CREATE USER %s' % env.DB_USER),
        ]
    cmd_list += [
        psql_cmd('alter user %s password \'%s\'' %
                 (env.DB_USER, env.DB_PASSWORD)),
        psql_cmd('ALTER ROLE %s SET client_encoding TO \'utf8\'' %
                 env.DB_USER),
        psql_cmd(('ALTER ROLE %s SET default_transaction_isolation' +
                  ' TO \'read committed\'') % env.DB_USER),
        psql_cmd('ALTER ROLE %s SET timezone TO \'UTC\'' % env.DB_USER),
        psql_cmd('GRANT ALL PRIVILEGES ON DATABASE %s TO %s' %
                 (database, env.DB_USER)),
    ]
    if local_run:
        cmd_list += [psql_cmd('ALTER ROLE %s CREATEDB' % env.DB_USER)]
    for i in cmd_list:
        if local_run:
            local(i)
        else:
            sudo(i)


def install_postgres_ru_locale():
    if fabtools.deb.is_installed('postgresql'):
        return
    sudo('apt update && apt upgrade')
    fabtools.require.deb.packages([
        'postgresql',
        'postgresql-contrib',
    ])
    sudo('locale-gen ru_RU.UTF-8')
    sudo('pg_dropcluster --stop 12 main')
    sudo('pg_createcluster --locale ru_RU.utf8 ' +
         ' --pgoption listen_addresses="localhost, 127.0.0.1" ' +
         '--start 12 main')


def install_local_postgres():
    '''Install local postgresql 9.5 and pgAdmin4'''
    print('TODO!')
    return
    lsb_release = local('lsb_release -cs', capture=True)
    cmd_list = [
        'sudo apt install postgresql postgresql-contrib',
        'wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc'
        + ' | sudo apt-key add -',
        'sudo sh -c \'echo "deb http://apt.postgresql.org/pub/repos/apt/' +
        ' %s-pgdg main" > /etc/apt/sources.list.d/pgdg.list\'' % lsb_release,
        'sudo apt-get update',
        'sudo apt-get upgrade',
        'sudo apt-get install postgresql-9.5 postgresql-contrib-9.5 pgadmin4',
    ]
    for i in cmd_list:
        local(i)


def psql_cmd(cmd, keys=''):
    return 'sudo -u postgres psql %s -c "%s;"' % (keys, cmd)
