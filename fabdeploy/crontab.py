import os
import sys

from fabric.api import env, local, run
from fabric.api import settings as fab_settings
from fabric.api import sudo
from fabric.contrib import files as fab_files
from fabtools import cron

from .helpers import text_print


class Crontabs():
    def __init__(self):
        if not hasattr(env, 'CRONTABS_SCRIPT_TPL_DIR'):
            print('Not set env.CRONTABS_SCRIPT_TPL_DIR. Exit...')
            sys.exit()

    def install(self):
        self.uninstall()
        text_print('cron install')
        run('mkdir %s' % env.CRONTABS_SCRIPT_DIR)
        for t in os.listdir(env.CRONTABS_SCRIPT_TPL_DIR):
            self.upload_template(t)

    @property
    def site_name(self):
        return env.SITE_NAME.replace('.', '_')

    def uninstall(self):
        text_print('cron uninstall')
        ls = run('ls /etc/cron.d/')
        for f in ls.split():
            if f.find(self.site_name) == 0:
                file = '/etc/cron.d/%s' % f
                sudo('rm -f %s' % file)
        with fab_settings(warn_only=True):
            run('rm -rf %s' % env.CRONTABS_SCRIPT_DIR)
            sudo('pkill -f manage.py')

    def uninstall_local(self):
        text_print('cron uninstall local')
        ls = local('ls /etc/cron.d/', capture=True)
        for f in ls.split():
            if f.find(self.site_name) == 0:
                file = '/etc/cron.d/%s' % f
                local('sudo rm -f %s' % file)
        with fab_settings(warn_only=True):
            local('rm -rf %s' % env.CRONTABS_SCRIPT_DIR)
            local(
                "kill -9 `ps -aux | grep manage.py | grep -v grep | awk '{print $2}'` || true"
            )

    def upload_template(self, name):
        file_ = '{dir}{file}'.format(dir=env.CRONTABS_SCRIPT_DIR, file=name)
        file_tpl = '{dir}{file}'.format(dir=env.CRONTABS_SCRIPT_TPL_DIR,
                                        file=name)

        fab_files.upload_template(
            file_tpl,
            file_,
            context=env,
            backup=False,
            mode='0775',
        )
        run('cat %s' % file_)
        timespec = ''
        timespec_marker = '# cron_timespec'
        pipenv = run('echo $PATH')
        with open(file_tpl) as f:
            for l in f.readlines():
                if timespec_marker in l:
                    timespec = l[len(timespec_marker):-1].strip()

        task_name = '%s_%s' % (self.site_name, name.split('.')[0])
        cron.add_task(task_name, timespec, env.user, file_, {'PATH': pipenv})


def crontab_install():
    '''Install cron scripts'''
    Crontabs().install()


def crontab_uninstall():
    '''Uninstall cron scripts'''
    Crontabs().uninstall()


def crontab_uninstall_local():
    '''Uninstall cron scripts in SELF server'''
    Crontabs().uninstall_local()
