$ORIGIN {{conf.DOMAIN}}.
$TTL 86400

; SOA Records
@        IN    SOA    hydrogen.ns.hetzner.com. dns.hetzner.com. 2020042701 86400 10800 3600000 3600

; NS Records
@        IN    NS    helium.ns.hetzner.de.
@        IN    NS    hydrogen.ns.hetzner.com.
@        IN    NS    oxygen.ns.hetzner.com.

; Gen Records {% for i in conf.DNS_RECORDS %}
{{'%-20s' % i.name}} IN    {{'%-5s' % i.type}} {{i.value}} {% endfor %}

{% if conf.DNS_RECORDS_OTHER_DATA%}
; Other
{{conf.DNS_RECORDS_OTHER_DATA}}
{% endif %}

; Date update
@        IN    TXT   "Date update {{date}}"
