[uwsgi]

uid = www-data
gid = www-data
chown-socket = www-data:www-data

chdir           = %(APP_ROOT)s
home            = %(VENV_PATH)s/
socket          = /tmp/%(APP_NAME)s.sock
module          = config.wsgi
stats           = /tmp/%(APP_NAME)s_stats.socket
logto           = %(LOGS_DIR)s%(APP_NAME)s.uwsgi.log

buffer-size     = 5000
