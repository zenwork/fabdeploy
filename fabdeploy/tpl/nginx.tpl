upstream {{SITE_NAME}} {
    server unix:///tmp/{{SITE_NAME}}.sock;
}


server {
    listen 80;
    server_name {{SITE_NAME}}
                {% if WWW_ENABLE %}www.{{SITE_NAME}}{% endif %}
                {% if ACCESS_SITE_BY_IP %}{{HOST_IP}}{% endif %}
                ;
    charset     utf-8;
    merge_slashes off;   

    gzip on;
    gzip_disable "msie6";
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript;


    client_max_body_size 800M;

    access_log /var/log/nginx/{{SITE_NAME}}.access.log;
    error_log  /var/log/nginx/{{SITE_NAME}}.error.log notice;

    location /static {
        alias {{APP_ROOT}}staticfiles/collectstatic ;
    }

    location /media {
        expires 1h;
        alias {{DATA_ROOT}}media ;
    }

    location / {
        rewrite ^(.*?)//+(.*?)$ $1/$2 permanent;

        {% if not ACCESS_SITE_BY_IP %}
		if ( $host !~* ^({{SITE_NAME}}|www.{{SITE_NAME}})$ ) {
		   return 444;
		}
        {% endif %}

        {% if HTTPS_ENABLE_TPL %}
        if ($ssl_protocol = "") {                                
            rewrite ^/(.*) https://$server_name/$1 permanent;
        }
        {% endif %}

        uwsgi_pass  {{SITE_NAME}};
        include uwsgi_params;

        {% if HTPASSWD_LOCK_SITE %}
        auth_basic           "Dev area";
        auth_basic_user_file {{HTPASSWD_FILE}}; 
        {% endif %}
    }

{% if HTTPS_ENABLE_TPL %}
    listen 443 ssl http2;
    ssl_certificate /etc/letsencrypt/live/{{SITE_NAME}}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/{{SITE_NAME}}/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
{% endif %}

}
