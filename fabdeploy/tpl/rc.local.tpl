#!/bin/sh -e
#
# copy to /etc/rc.local

mkdir -p /var/run/sshd

%(UWSGI_PATH)s --emperor %(UWSGI_VASSALS_DIR)s --uid www-data --gid www-data --daemonize /var/log/uwsgi-emperor.log

exit 0
