import os
import socket
import sys
import tempfile
import time
from contextlib import contextmanager

from fabric.api import cd, env, get, hide, local, prefix
from fabric.api import reboot as fab_reboot
from fabric.api import run, settings, sudo
from fabric.colors import green
from fabric.contrib import files as fab_files
from fabtools import require
from pyfiglet import Figlet as _Figlet


def text_print(string):
    width = 120
    print(green('#' * width))
    print('')
    f = _Figlet(font='banner3', width=width)
    print(green(f.renderText(string)))


def get_self_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def get_file_content(path):
    if not fab_files.exists(path):
        return False
    tmp_file = tempfile.TemporaryFile()
    get(path, tmp_file)
    tmp_file.seek(0)
    content = tmp_file.read()
    content = content.decode()
    return str(content)


def check_access_server_by_key_file():
    flag = bool(env.abort_on_prompts)
    env.abort_on_prompts = True
    with settings(
            hide('output', 'running', 'warnings', 'stdout', 'stderr'),
            warn_only=True,
    ):
        try:
            run('ls /')
            access_by_key = True
        except SystemExit:
            access_by_key = False
    env.abort_on_prompts = flag
    return access_by_key


def get_server_installed_datetime():
    if env.host_string == get_self_ip_address():
        return local('cat ~/.server_installed', capture=True).strip()
    if not check_access_server_by_key_file():
        return False
    inf = get_file_content('.server_installed')
    if inf:
        return inf.strip()
    return False


def reboot():
    text_print('reboot')
    require.deb.packages(['tmux'])
    fab_reboot(command='tmux new-session -d "sleep 1; reboot;"')
    time.sleep(10)


def create_uwsgi_conf():
    file_ = '{env.UWSGI_VASSALS_DIR}{env.APP_NAME}.ini'.format(env=env)
    env.VENV_PATH = get_pipenv_path()
    fab_files.upload_template(
        get_template_path('uwsgi.tpl'),
        file_,
        context=env,
    )
    if not equal_new_file_and_bak(file_):
        run('cat %s' % file_)


##############################################################################


class CreateNginxConf:
    def __init__(self, ssl=True):
        self.ssl = True
        self.file_ = '{env.NGINX_CONF_DIR}{env.APP_NAME}.conf'.format(env=env)
        self.tpl = 'nginx.tpl'
        self.f_path = get_template_path(self.tpl)
        self.tpl_dir = self.f_path[0:-len(self.tpl)]
        self.create()
        self.reload()

    def create(self):
        # first step
        if self.ssl and env.HTTPS_ENABLE:
            env.HTTPS_ENABLE_TPL = True
        else:
            env.HTTPS_ENABLE_TPL = False
        self.upload()
        # step two
        if equal_new_file_and_bak(self.file_):
            return
        if env.HTTPS_ENABLE_TPL and not self.check_cert_set_in_conf():
            env.HTTPS_ENABLE_TPL = False
            self.upload()

    def check_cert_set_in_conf(self):
        return fab_files.exists(self.get_cert_path(), use_sudo=True)

    def get_cert_path(self):
        conf = get_file_content(self.file_)
        for i in conf.split('\n'):
            if 'privkey.pem' in i:
                cert_str = i
        cert_p = cert_str.split(';')[0].split(' ')[-1].strip()
        return cert_p

    def upload(self):
        fab_files.upload_template(
            self.tpl,
            self.file_,
            context=env,
            use_jinja=True,
            template_dir=self.tpl_dir,
        )

    def reload(self):
        if not equal_new_file_and_bak(self.file_):
            run('cat %s' % self.file_)
            sudo('nginx -t')
            text_print('nginx reload...')
            sudo('nginx -s reload')
            text_print('ok')


def create_nginx_conf(ssl=True):
    '''Create nonf in jinja template'''
    CreateNginxConf(ssl)


##############################################################################


def lazy_mkdir(path):
    if not fab_files.exists(path):
        run('mkdir -pv %s' % path)


def chown_dir(directory='.'):
    sudo('chown -R {user}:{user} {directory}'.format(
        user=env.user,
        directory=directory,
    ))


def equal_new_file_and_bak(file_path):
    '''
    Проверяет есть ли бекап интересующего файла.
    Нужно для проверки необходимости перезагружать службы
    '''
    print('Compare file and backup')
    file_old_path = '%s.bak' % file_path

    if not fab_files.exists(file_old_path):
        return False

    tmp_file = tempfile.TemporaryFile()
    get(file_path, tmp_file)
    tmp_file.seek(0)
    new = tmp_file.read()

    tmp_file = tempfile.TemporaryFile()
    get(file_old_path, tmp_file)
    tmp_file.seek(0)
    old = tmp_file.read()
    return old == new


def get_pipenv_path():
    with cd(env.APP_ROOT):
        return run('pipenv --venv')


@contextmanager
def venv_context():
    '''
    Контест витруального окружения для запуска питона
    '''
    with cd(env.APP_ROOT):
        with prefix('source $(pipenv --venv)/bin/activate'):
            with prefix('export LANG=C.UTF-8 && export LC_ALL=C.UTF-8'):
                yield


def get_template_path(name):
    if hasattr(env, 'TEMPLATES_DIR'):
        path = os.path.normpath(os.path.join(env.TEMPLATES_DIR, name))
        if os.path.exists(path):
            return path
    return os.path.normpath(
        os.path.join(os.path.dirname(__file__), 'tpl', name))


def confirm(question, first=True):
    yes = {'yes', 'y', 'ye'}
    no = {'no', 'n'}
    q = ('%s [y/n]: ' % question) if first else ''
    choice = input(q).lower()
    if choice in yes:
        return True
    elif choice in no:
        return False
    else:
        sys.stdout.write("Please respond with 'yes' or 'no': ")
        return confirm(question, False)


def print_matrix_table(table, padding=2):
    max_len = {i: 0 for i in range(len(table[0]))}
    for row in table:
        for k, i in enumerate(row):
            max_len[k] = max(max_len[k], len(str(i)))
    tpl = ''
    for k, i in max_len.items():
        tpl += '{%s:%s} ' % (k, i + padding)
    for row in table:
        line = tpl.format(*map(str, row))
        print(line)
