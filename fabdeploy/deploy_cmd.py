from fabric.api import env, prompt

from .crontab import (crontab_install, crontab_uninstall,
                      crontab_uninstall_local)
from .helpers import confirm, text_print
from .postgres import install_local_postgres
from .redis import install_redis
from .upserver import UpServer


class DeployCmdMixin():
    def __add_cmd(self):
        self.cmd_list.append(self.sync)
        self.cmd_list.append(crontab_install)
        self.cmd_list.append(crontab_uninstall)
        self.cmd_list.append(crontab_uninstall_local)
        self.cmd_list.append(self.first_deploy_to_server)
        self.cmd_list.append(UpServer)
        self.cmd_list.append(install_local_postgres)

        self.check_enabled_cmd()
        self.sort_cmd()

    def sort_cmd(self):
        def sort_list(cmd):
            return cmd.__doc__ or cmd.__name__

        self.cmd_list.sort(key=sort_list)

    def check_enabled_cmd(self):
        if not self.cmd_enabled:
            return
        new_cmd_list = []
        self.disabled_cmd = []
        for i in self.cmd_list:
            if i.__name__ in self.cmd_enabled:
                new_cmd_list.append(i)
            else:
                self.disabled_cmd.append(i)
        self.cmd_list = new_cmd_list
        self.cmd_list.append(self.show_disabled_cmd)

    def show_disabled_cmd(self):
        '''Show disabled cmd'''
        for i in self.disabled_cmd:
            cmd = '"{cmd.__name__}" -- {cmd.__doc__}'.format(cmd=i)
            print(cmd)

    def test_cmd(self):
        '''Test_cmd docstring'''
        text_print('test cmd')

    def check_cmd_flags(self):
        self.__add_cmd()
        cmd = self.args[0] if self.args else 'cmd_prompt'
        try:
            cmd = int(cmd)
        except ValueError:
            cmd = str(cmd)

        if isinstance(cmd, int):
            try:
                cmd = self.cmd_list[cmd]
            except IndexError:
                print('Command number error')
                return
            self.__confirm_cmd_run(cmd)
        elif cmd == 'cmd_prompt':
            print('-' * 80)
            print('Server: %s [%s]' % (env.SITE_NAME, env.host_string))
            print('-' * 80)
            for i, cmd in enumerate(self.cmd_list):
                if cmd.__doc__:
                    print('%s. %s [%s]' % (i, cmd.__doc__, cmd.__name__))
                else:
                    print('%s. %s' % (i, cmd.__name__))
            cmd_number = prompt('Print number command: ', validate=int)
            cmd = self.cmd_list[cmd_number]
            self.__confirm_cmd_run(cmd)
        else:
            for i in self.cmd_list:
                if i.__name__ == cmd:
                    i()
                    return
            print('Error. Command not found')

    def __confirm_cmd_run(self, cmd):
        if confirm('%s. Run?' %
                   (cmd.__doc__ if cmd.__doc__ else cmd.__name__)):
            cmd()
