import sys

from fabric.api import env, local, run, settings, sudo


class CertbootToolsMixin:
    def copy_letsencrypt_certs_from_old_server(self):
        '''Copy letsencrypt certs from server. Set env OLD_SERVER_IP'''
        if not hasattr(env, 'OLD_SERVER_IP'):
            print('Error. Not set OLD_SERVER_IP in env. Exit...')
            sys.exit()
        local('rm -rf letsencrypt')
        with settings(host_string=env.OLD_SERVER_IP):
            sudo('rsync -a --progress /etc/letsencrypt %s' % env.USER_HOME)
            sudo('chown -R %s:%s %sletsencrypt' %
                 (env.user, env.user, env.USER_HOME))
            local('rsync -a --progress {env.user}@{env.host_string}'
                  ':letsencrypt .'.format(env=env))
            run('rm -rf %sletsencrypt' % env.USER_HOME)
        local('rsync -a --progress letsencrypt '
              '{env.user}@{env.host_string}:{env.USER_HOME}'.format(env=env))
        local('rm -rf letsencrypt')
        sudo('rsync -a letsencrypt /etc')
        sudo('chown -R root:root /etc/letsencrypt')
        run('rm -rf %sletsencrypt' % env.USER_HOME)

    def create_certbot_certificat(self):
        '''Install certbot certificat'''
        if not env.HTTPS_ENABLE:
            print('Error. Not enable https [env.HTTPS_ENABLE]. Eixt...')
            sys.exit()

        # TODO optimize logic
        if env.WWW_ENABLE:
            cmd = 'certbot delete -n --agree-tos --cert-name ' \
                '{site} www.{site}'.format(site=env.SITE_NAME)
        else:
            cmd = 'certbot delete -n --agree-tos --cert-name ' \
                '{site}'.format(site=env.SITE_NAME)
        with settings(warn_only=True):
            sudo(cmd)

        if env.WWW_ENABLE:
            cmd = 'certbot --nginx -n --agree-tos --email {mail} ' \
                '-d {site} -d www.{site}'.format(
                    site=env.SITE_NAME,
                    mail=env.EMAIL_ADMIN,
                )
        else:
            cmd = 'certbot --nginx -n --agree-tos --email {mail} ' \
                '-d {site}'.format(
                    site=env.SITE_NAME,
                    mail=env.EMAIL_ADMIN,
                )
        with settings(warn_only=True):
            sudo(cmd)

        if env.WWW_ENABLE:
            cmd = 'certbot --nginx rollback ' \
                '-d {site} -d www.{site}'.format(
                    site=env.SITE_NAME,
                    mail=env.EMAIL_ADMIN,
                )
        else:
            cmd = 'certbot --nginx rollback ' \
                '-d {site}'.format(
                    site=env.SITE_NAME,
                    mail=env.EMAIL_ADMIN,
                )
        with settings(warn_only=True):
            sudo(cmd)

    def install_certbot(self):
        with settings(warn_only=True):
            # for ubuntu 16.04
            # sudo('yes | apt install software-properties-common')
            # sudo('sudo add-apt-repository ppa:certbot/certbot -y')
            # sudo('yes | apt update')
            # sudo('yes | apt install python-certbot-nginx')
            # for ubuntu 20.04
            # sudo('yes| apt install certbot python3-certbot-nginx')
            sudo('yes| apt install snapd')
            sudo('yes| snap install core')
            sudo('yes| snap refresh core')
            sudo('yes| snap install --classic certbot')
            sudo('ln -s /snap/bin/certbot /usr/bin/certbot')
