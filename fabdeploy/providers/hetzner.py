import datetime
import json
import sys
import time

import requests
from fabric.api import env, run, settings
from hcloud import Client
from hcloud.datacenters.client import BoundDatacenter
from hcloud.hcloud import APIException
from hcloud.images.domain import Image
from hcloud.server_types.domain import ServerType
from jinja2 import Template

from ..conf import env_conf, get_var_in_env_from_user
from ..helpers import get_server_installed_datetime, get_template_path
from ..upserver import UpServer


class HetznerDNS:
    '''
    Upload DNS zone conf to Hetzner DNS servers.
    Add env:
        HETZNER_TOKEN_DNS
    For init add conf dict:
        DOMAIN
        DNS_RECORDS = [
            {
                'type': 'A, MX, etc...',
                'name': '@ - for root, www, mail for www, mail',
                'value': 'ip',
            },
        ]
        DNS_RECORDS_OTHER_DATA  - for add more text to end conf file
    Run HetznerDNS().install()
    '''
    ttl_default = 86400
    base_url = "https://dns.hetzner.com/api/v1"
    dns_zone_tpl = 'dns_zone.tpl'

    def __init__(self, conf, debug=False):
        self.conf = conf
        self.debug = debug

    @property
    def token(self):
        return get_var_in_env_from_user('HETZNER_TOKEN_DNS')

    def install(self):
        if not self.validate_zone_conf():
            return
        if self.zone_id:
            self.delete_zone()
        self.create_zone()
        self.import_zone_conf()

    @property
    def zone_conf(self):
        tpl = Template(open(get_template_path(self.dns_zone_tpl)).read())
        other_data = self.conf.get('DNS_RECORDS_OTHER_DATA', False)
        zone = tpl.render(
            conf=self.conf,
            other_data=other_data,
            date=datetime.datetime.now(),
        )
        zone = [i for i in zone.split('\n') if i.strip() != '']
        return '\n'.join(zone)

    @property
    def domain(self):
        name = self._get_from_conf('$ORIGIN')
        return name[:-1] if name[-1] == '.' else name

    def _get_from_conf(self, var_name):
        for i in self.zone_conf.split('\n'):
            if i.startswith(var_name):
                return i.split()[1].strip()
        return False

    @property
    def ttl(self):
        ttl_conf = self._get_from_conf('$TTL')
        return int(ttl_conf) if ttl_conf else self.ttl_default

    @property
    def zone_id(self):
        if not hasattr(self, '_zone_id') or not self._zone_id:
            self._zone_id = self.get_zone_id()
        return self._zone_id

    def get_zone_id(self):
        rsp = self.request('GET', self.base_url + '/zones')
        if not rsp or 'zones' not in rsp or not rsp['zones']:
            print('Zone not found')
            return False
        for i in rsp['zones']:
            if i['name'] == self.domain:
                return i['id']
        return False

    def delete_zone(self):
        if not self.zone_id:
            print('No zone, no delete')
            return False
        url = '%s/zones/%s' % (self.base_url, self.zone_id)
        rsp = self.request('DELETE', url)
        if rsp:
            print('Zone %s delete' % self.domain)
            self._zone_id = False
            return True
        print("Zone NO delete")
        return False

    def create_zone(self):
        rsp = self.request(
            'POST',
            self.base_url + '/zones',
            {
                'name': self.domain,
                'ttl': self.ttl,
            },
        )
        if rsp:
            print('Zone %s create' % self.domain)

    def import_zone_conf(self):
        url = '%s/zones/%s/import' % (self.base_url, self.zone_id)
        rsp = self.request('POST', url, self.zone_conf)
        if rsp:
            print('#' * 79)
            print(self.zone_conf)
            print('#' * 79)
            print('Zone conf import')
        else:
            print('Zone conf FAIL import!')

    def validate_zone_conf(self):
        rsp = self.request(
            'POST',
            self.base_url + '/zones/file/validate',
            self.zone_conf,
        )
        if rsp:
            print('Zone conf valid')
        else:
            print('Zone conf FAIL validation')
        return bool(rsp)

    def request(self, method, url, data=False):
        rq = {
            'headers': {
                "Auth-API-Token": self.token,
            }
        }
        if data:
            if isinstance(data, str):
                rq['headers']['Content-Type'] = 'text/plain'
                rq['data'] = data
            else:
                rq['headers']['Content-Type'] = "application/json"
                rq['data'] = json.dumps(data)
        try:
            rsp = requests.request(method, url, **rq)
            if self.debug:
                print('%s %s' % (method, url))
                print('status: %s' % rsp.status_code)
                print('data: %s' % data)
                try:
                    print(json.dumps(rsp.json(), indent=4, sort_keys=True))
                except json.decoder.JSONDecodeError:
                    print(rsp.text)
            if not rsp.text:
                return rsp.ok
            try:
                return rsp.json()
            except json.decoder.JSONDecodeError:
                return False
        except requests.exceptions.RequestException:
            return False


class HetznerServer:
    '''
    Hetzner server provider
    '''
    watch_seconds = 10
    default_server_type = 'cx11'
    default_server_os = 'ubuntu-16.04'

    def __init__(self,
                 name,
                 volume_size_gb=False,
                 backup_volume=False,
                 server_type=False,
                 server_os=False,
                 datacenter=False,
                 for_django=True):
        self.backup_volume = backup_volume
        self.name = name
        self.volume_size_gb = volume_size_gb
        self.for_django = for_django
        self.datacenter_str = datacenter
        self.server_type = server_type if server_type \
            else self.default_server_type
        self.server_os = server_os if server_os else self.default_server_os

    @property
    def token(self):
        return get_var_in_env_from_user('HETZNER_TOKEN')

    @property
    def client(self):
        if not hasattr(self, '_client'):
            self._client = Client(token=self.token)
        return self._client

    @property
    def date_create(self):
        env.SITE_NAME = self.ip
        env_conf()
        print('Get date create server from %s' % self.name)
        return get_server_installed_datetime()

    @property
    def ready(self):
        if not self.server:
            return False
        return bool(self.date_create)

    @property
    def name_volume(self):
        return '%s-volume' % self.name

    @property
    def root_password(self):
        if not hasattr(self, '_root_password'):
            rq = self.server.reset_password()
            self._root_password = rq.root_password
            print('Root password reset. New password: %s' %
                  self._root_password)
        return self._root_password

    def reset_root_password(self):
        if hasattr(self, '_root_password'):
            del self._root_password
        if hasattr(self, '_client'):
            del self._client
        return self.root_password

    @property
    def server(self):
        try:
            return self.client.servers.get_by_name(self.name)
        except APIException as e:
            print('Error: "%s"". Exit...' % e.message)
            sys.exit()

    @property
    def volume(self):
        return self.client.volumes.get_by_name(self.name_volume)

    @property
    def ip(self):
        if self.server:
            return self.server.data_model.public_net.ipv4.ip
        return False

    def up(self):
        get_var_in_env_from_user('NEW_USER_PASSWORD')
        self.delete()
        self.create()
        self.configure()

    def delete(self):
        print('Delete server "%s"' % self.name)
        if self.volume:
            self.delete_volume()
        self.delete_server()

    def create(self):
        self.create_server()
        self.reset_root_password()
        if self.volume_size_gb:
            self.create_volume()
            self.mount_volume()
            self.attach_volume_to_home()
        self.reboot()
        print('#' * 79)
        print('Server ready')
        self.get_ssh_connection_info()
        print('#' * 79)

    def configure(self):
        connect = {
            'SITE_NAME': self.ip,
            'password': self.root_password,
        }
        with settings(**connect):
            UpServer(for_django=self.for_django)

    def delete_server(self):
        if self.server:
            self.server.delete()
            self._watch('Server "%s" delete' % self.name)

    def delete_volume(self):
        if not self.server.data_model.volumes:
            print('No volume for delete')
            return
        self.volume.detach()
        self._watch('Volume detached')

        if self.backup_volume:
            self._backup_volume()
        else:
            self._delete_volume()

    def _backup_volume(self):
        name = self._get_backup_volume_name()
        self.volume.update(name=name)
        print('Volume backup to "%s"' % name)

    def _get_backup_volume_name(self):
        date = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        return '{name}-backup-{date}'.format(name=self.name_volume, date=date)

    def _delete_volume(self):
        if self.volume:
            self.volume.delete()
            self._watch('Volume "%s" delete' % self.name_volume)

    def create_server(self):
        print('Create server "%s"...' % self.name)
        try:
            rsp = self.client.servers.create(
                name=self.name,
                server_type=ServerType(name=self.server_type),
                image=Image(name=self.server_os),
                datacenter=self.datacenter,
            )
        except APIException as e:
            print('Error create "{name}": {message}'.format(
                name=self.name,
                message=e.message,
            ))
            return False
        self._root_password = rsp.root_password
        self._watch('Server create', 60)
        return True

    @property
    def datacenter(self):
        if not self.datacenter_str:
            return None
        try:
            return self.client.datacenters.get_by_name(self.datacenter_str)
        except APIException:
            print('Error datacenter name. Exit...')
            sys.exit()

    def create_volume(self):
        location = self.server.data_model.datacenter.data_model.location
        try:
            self.client.volumes.create(
                size=self.volume_size_gb,
                name=self.name_volume,
                location=location,
            )
        except APIException as e:
            print('Error create "{name}": {message}'.format(
                name=self.name_volume,
                message=e.message,
            ))
            return False
        self._watch('Volume create')
        return True

    def mount_volume(self):
        self.volume.attach(self.server)
        self._watch('Volume mounted')

    def get_ssh_connection_info(self):
        print('ssh root@%s' % self.ip)
        print('Password: %s' % self.root_password)

    def attach_volume_to_home(self):
        vol_id = self.volume.data_model.linux_device
        cmd = ('echo "{vol_id} '
               '/home ext4 discard,nofail,defaults 0 0" >> /etc/fstab').format(
                   vol_id=vol_id)
        connect = {
            'user': 'root',
            'host_string': self.ip,
            'password': self.root_password,
        }
        with settings(**connect):
            run('mkfs -F %s' % vol_id)
            run(cmd)
            print('Volume attach to home')

    def reboot(self):
        self.server.reboot()
        self._watch('Server reboot...', 30)

    ##########################################################################

    def _watch(self, message=False, watch_seconds=False):
        watch = watch_seconds if watch_seconds else self.watch_seconds
        message = '%s. ' % message if message else ''
        print('%sWatch %s seconds...' % (message, watch))
        time.sleep(watch)
