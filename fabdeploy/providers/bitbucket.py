import json
import math

import requests
from fabric.api import local

from ..conf import get_var_in_env_from_user


class BitbucketAccessKeys:
    '''Helper for add and clean bitbucket deploy keys'''

    host = "https://api.bitbucket.org"
    headers = {"content-type": "application/json"}
    api_point_all_keys_tpl = "/2.0/repositories/{workspace}/{repo}/deploy-keys"

    def __init__(self, repo=False, workspace=False):

        self.user = get_var_in_env_from_user('GIT_REPO_USER', hide_value=False)
        self.password = get_var_in_env_from_user('GIT_REPO_PASSWORD')
        self.repo = repo if repo else self._repo
        self.workspace = workspace if workspace else self._workspace

    @property
    def _repo(self):
        return self._git[1]

    @property
    def _workspace(self):
        return self._git[0]

    @property
    def _git(self):
        origin = local('git remote get-url origin', capture=True)
        origin = origin.split(':')[1].split('/')
        workspace = origin[0]
        repo = origin[1][:-4]
        return workspace, repo

    def add_key(self, key, label=False):
        '''Add deploy key to bitbucket'''
        if not label:
            _, label = self._prepare_key(key)
        data = {'key': key, 'label': label}
        r = requests.post(self.api_point_all_keys,
                          auth=self.auth,
                          headers=self.headers,
                          data=json.dumps(data))
        if r.ok:
            print('Key %s added' % label)
        else:
            print('Error add key %s' % label)
            print('Response: %s' % r.text)

    def _prepare_key(self, key):
        '''Split pub key o tuple - (key, label)'''
        pub_key = key[:key.find(' ', 20)]
        pub_label = key[key.find(' ', 20) + 1:]
        return pub_key, pub_label

    def delete_all_keys(self):
        r = requests.get(self.api_point_all_keys,
                         auth=self.auth,
                         headers=self.headers)
        if not r.ok:
            print('Error bitbucket api connection. Exit...')
            sys.exit()

        data = r.json()
        count_steps = math.ceil(data['size'] / data['pagelen'])
        for i in range(1, count_steps + 1):
            print('Delete bitbucket deploy keys step %s' % i)
            self._delete_keys_step()

    def _delete_keys_step(self):
        r = requests.get(self.api_point_all_keys,
                         auth=self.auth,
                         headers=self.headers)
        keys = r.json()['values']
        keys_api = [i['links']['self']['href'] for i in keys]
        for i in keys_api:
            r = requests.delete(i, auth=self.auth, headers=self.headers)
            print(i)

    @property
    def auth(self):
        return requests.auth.HTTPBasicAuth(self.user, self.password)

    @property
    def api_point_all_keys(self):
        return self.host + self.api_point_all_keys_tpl.format(
            workspace=self.workspace, repo=self.repo)
