import socket
import subprocess
import sys
from getpass import getpass

from fabric.api import env, local, prompt


class ConfFabficEnvMixin():
    def conf_env(self):
        if env.user == local('whoami', capture=True):
            env.user = 'webdev'

        self.add_env_attr('GIT_BRANCH', self.get_git_branch())

        if 'HOST_IP' in env:
            env.host_string = env.HOST_IP
        else:
            try:
                env.host_string = socket.gethostbyname(env.SITE_NAME)
            except socket.gaierror:
                print("Error ip for %s. Set env.HOST_IP or conf DNS. Exit..." %
                      env.SITE_NAME)
                sys.exit()

        self.add_env_attr('ACCESS_SITE_BY_IP', False)
        self.add_env_attr('HTTPS_ENABLE', True)
        self.add_env_attr('WWW_ENABLE', False)

        self.add_env_attr('DB_USER', env.user)
        self.add_env_attr('DB_PASSWORD', 'password')

        self.add_env_attr('USER_HOME', '/home/%s/' % env.user)
        self.add_env_attr('HOME_DIR', '/home/%s/www/' % env.user)
        self.add_env_attr('BACKUP_DIR',
                          '%sbackup-%s/' % (env.HOME_DIR, env.SITE_NAME))
        self.add_env_attr('PROJECT_ROOT', '%s%s/' % (
            env.HOME_DIR,
            env.SITE_NAME,
        ))
        self.add_env_attr('APP_ROOT', env.PROJECT_ROOT + 'app/')
        self.add_env_attr('DATA_ROOT', env.PROJECT_ROOT + 'data/')
        self.add_env_attr('DATA_LOCAL_ROOT', '../data/')
        self.add_env_attr('APP_NAME', env.SITE_NAME)
        self.add_env_attr('NGINX_CONF_DIR', '/etc/nginx/conf.d/')
        self.add_env_attr('UWSGI_VASSALS_DIR', '/etc/uwsgi-vassals/')
        self.add_env_attr('LOGS_DIR', '/var/log/')
        self.add_env_attr('EMAIL_ADMIN', 'example@example.com')
        self.add_env_attr('CRONTABS_SCRIPT_DIR',
                          '/home/%s/crontabs/' % env.user)
        self.add_env_attr('CRONTABS_SCRIPT_TPL_DIR', 'crontabs/')

        self.add_env_attr('DJANGO_SETTINGS_FILE', 'config.settings.prod')
        self.add_env_attr(
            'HTPASSWD_FILE',
            '{env.HOME_DIR}.{env.SITE_NAME}.htpasswd'.format(env=env))
        self.add_env_attr('HTPASSWD_LOCK_SITE', False)
        self.add_env_attr('RSA_PUB_KEY_PATH', '~/.ssh/id_rsa.pub')

    @staticmethod
    def add_env_attr(name, value):
        if not hasattr(env, name):
            setattr(env, name, value)

    def get_git_branch(self):
        branch = subprocess.check_output([
            'git',
            'rev-parse',
            '--abbrev-ref',
            'HEAD',
        ]).decode().strip()
        return branch


def env_conf():
    conf = ConfFabficEnvMixin()
    conf.conf_env()


def get_var_in_env_from_user(var_name, hide_value=True):
    if var_name not in env:
        text = 'Enter value for "%s": ' % var_name
        env[var_name] = getpass(text) if hide_value else prompt(text)
    return env[var_name]
