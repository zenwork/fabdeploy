from fabric.api import cd, env, local, run, sudo
from fabtools import require

from .crontab import crontab_install
from .helpers import create_nginx_conf, lazy_mkdir, text_print
from .providers.bitbucket import BitbucketAccessKeys


class FirstDeployMixin():
    def first_deploy_to_server(self):
        '''First deploy project to server'''
        # create_nginx_conf()
        self.before_first_deploy()
        text_print('first deploy')
        self.create_project_dirs()
        self.set_env_git()
        self.add_deploy_key()
        self.clone_app_sourse()
        self.project_venv_initial()
        self.sync_local_data_to_server()
        self.create_nginx_conf_first_step()
        if env.HTTPS_ENABLE:
            self.create_certbot_certificat()
        self.after_first_deploy()
        self.base_deploy()
        crontab_install()

    def create_nginx_conf_first_step(self):
        create_nginx_conf(ssl=False)

    def before_first_deploy(self):
        '''For inheritance class'''

    def after_first_deploy(self):
        '''For inheritance class'''

    def set_env_git(self):
        if not hasattr(env, 'GIT'):
            env.GIT = local('git remote get-url origin', capture=True)

    def create_project_dirs(self):
        lazy_mkdir(env.HOME_DIR)
        run('rm -rf %s' % env.PROJECT_ROOT)
        lazy_mkdir(env.PROJECT_ROOT)

    def add_deploy_key(self):
        '''Insert deploy key in bitbucket repo'''
        pub_key = run('cat ~/.ssh/id_rsa.pub')
        bb = BitbucketAccessKeys()
        bb.add_key(pub_key)

    def clone_app_sourse(self):
        run('git clone {git} {path}'.format(
            git=env.GIT,
            path=env.PROJECT_ROOT,
        ))
        with cd(env.APP_ROOT):
            run('git checkout %s' % env.GIT_BRANCH)

    def sync_local_data_to_server(self):
        text_print('sync local to server')
        require.deb.packages(['rsync'])
        # sudo('usermod -aG www-data %s' % env.user)
        cmd = ('rsync --progress --delete -av ' +
               '{local_data} {env.user}@{env.host_string}:{env.DATA_ROOT} '
               ).format(
                   env=env,
                   local_data=env.DATA_LOCAL_ROOT,
               )
        local(cmd)
        sudo('find %s -type d | xargs chmod 775' % env.DATA_ROOT)
        sudo('chown -R www-data:www-data %s' % env.DATA_ROOT)

    def project_venv_initial(self):
        with cd(env.APP_ROOT):
            file_ = 'Pipfile.lock'
            run('cp {file} {file}.bak'.format(file=file_))
            run('pipenv install --deploy')
