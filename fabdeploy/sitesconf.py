import socket

from fabric.exceptions import NetworkError

from .helpers import print_matrix_table
from .providers.hetzner import HetznerDNS


class ServersConf:
    '''
    For init add servers provider list
    '''
    def __init__(self, servers):
        self.all = servers

    def __getattr__(self, name):
        for i in self.all:
            if i.name == name:
                return i

    def __getitem__(self, name):
        return self.__getattr__(name)

    def get_status_list(self):
        '''Return list of dict servers status.
           NAME, TYPE, VOLUME, IP, DATE CREATE, STATUS
        '''
        rows = []
        for i in self.all:
            inf = {
                'name': i.name,
                'status': '??',
                'volume':
                '%sGB' % i.volume_size_gb if i.volume_size_gb else '-',
                'ip': '-',
                'date_create': '',
                'type': i.server_type
            }
            s = i.server
            if s:
                inf['ip'] = i.ip
                try:
                    date_create = i.date_create
                except NetworkError:
                    date_create = False
                if date_create:
                    inf['date_create'] = date_create
                    inf['status'] = 'ready'
                else:
                    inf['status'] = 'not ready'
            rows.append(inf)
        return rows

    def print_status(self):
        table = [[
            'NAME',
            'TYPE',
            'VOLUME',
            'IP',
            'DATE CREATE',
            'STATUS',
        ]]
        for i in self.get_status_list():
            table.append([
                i['name'],
                i['type'],
                i['volume'],
                i['ip'],
                i['date_create'],
                i['status'],
            ])
        print('#' * 82)
        print('SERVERS STATUS\n')
        print_matrix_table(table)
        print('#' * 82)


class Site:
    '''
    Conf for one site
    other_dns - "raw" string for dns zon conf flie

    '''
    def __init__(self,
                 domain,
                 server_name,
                 other_dns=False,
                 set_dns_ptr=False):
        self.domain = domain
        self.server_name = server_name
        self.other_dns = other_dns
        self.set_dns_ptr = set_dns_ptr

    def __repr__(self):
        return '%s [%s]' % (self.domain, self.server_name)

    @property
    def subdomain(self):
        return '.'.join(self.domain.split('.')[:-2])

    @property
    def ip(self):
        server = self.server
        if not server:
            return False
        return server.ip

    @property
    def server(self):
        return self.servers[self.server_name]

    @property
    def ip_str(self):
        ip = self.ip
        return ip if ip else '-'

    @property
    def ready(self):
        r1 = bool(self.server)
        r2 = self.ip == self.real_ip
        return r1 and r2

    @property
    def real_ip(self):
        try:
            return socket.gethostbyname(self.domain)
        except socket.gaierror:
            return '-'

    @property
    def real_ip_str(self):
        ip = self.real_ip
        return ip if ip else '-'

    @property
    def status(self):
        return 'ready' if self.ready else 'not ready'


class Sites:
    def __init__(self, sites, servers):
        self.all = sites
        for i in self.all:
            i.servers = servers

    def print_status(self):
        print('#' * 82)
        print('DNS STATUS\n')
        table = [[
            'DOMAIN',
            'SERVER',
            'IP',
            'REAL IP',
            'STAUS',
        ]]
        for site in self.all:
            table.append([
                site.domain,
                site.server_name,
                site.ip,
                site.real_ip_str,
                site.status,
            ])
        print_matrix_table(table)
        print('#' * 82)

    def status(self):
        self.servers.print_status()
        self.print_status()

    def update_dns(self):
        for i in self.roots:
            dns = self.get_dns_zone_obj(i)
            if self.ready:
                dns.install()

    def update_rdns(self):
        for i in self.all:
            if not i.set_dns_ptr:
                continue
            ip = i.server.server.data_model.public_net.ipv6.ip.replace(
                '/64', '1')
            i.server.server.change_dns_ptr(ip, i.domain)
            print('Set DNS PTR %s -> %s' % (ip, i.domain))

    def show_dns_conf(self):
        for i in self.roots:
            dns = self.get_dns_zone_obj(i)
            print(dns.zone_conf)
            print('#' * 79)

    @property
    def ready(self):
        for i in self.all:
            if not i.server.ready:
                print('Error. Server "%s" not ready')
                return False
        return True

    def get_dns_zone_obj(self, site):
        conf = {
            'DOMAIN': site.domain,
            'DNS_RECORDS': [
                {
                    'type': 'A',
                    'name': '@',
                    'value': site.ip,
                },
            ],
        }
        for i in self._get_childrens(site):
            conf['DNS_RECORDS'].append({
                'type': 'A',
                'name': i.subdomain,
                'value': i.ip,
            })
        if site.other_dns:
            conf['DNS_RECORDS_OTHER_DATA'] = site.other_dns
        dns = HetznerDNS(conf)
        return dns

    @property
    def roots(self):
        roots = []
        for i in self.all:
            if self._is_root(i.domain):
                roots.append(i)
        return roots

    def _get_childrens(self, site):
        childrens = []
        for i in self.all:
            if i in self.roots:
                continue
            if i.domain.endswith(site.domain):
                childrens.append(i)
        return childrens

    def _is_root(self, name):
        return len(name.split('.')) < 3
