from fabric.api import cd, env, local, run, settings, sudo
from fabtools import require

from .certbot import CertbootToolsMixin
from .conf import ConfFabficEnvMixin
from .deploy_cmd import DeployCmdMixin
from .firstdeploy import FirstDeployMixin
from .helpers import (create_nginx_conf, create_uwsgi_conf,
                      equal_new_file_and_bak, text_print, venv_context)


class Deploy(ConfFabficEnvMixin, FirstDeployMixin, DeployCmdMixin,
             CertbootToolsMixin):
    cmd_list = []
    cmd_enabled = []

    def __init__(self, *args):
        self.args = args
        self.conf_env()
        self.cmd_list.append(self.base_deploy)
        self.cmd_list.append(self.create_certbot_certificat)
        self.cmd_list.append(self.copy_letsencrypt_certs_from_old_server)
        self.cmd_list.append(self.create_conf)
        self.cmd_list.append(self.add_deploy_key)
        self.check_cmd_flags()

    def base_deploy(self):
        '''BASE deploy to server'''
        self.before_deploy()
        self.sync()
        text_print('deploy...')
        self.git_load()
        self.pip_sync()
        self.collectstatic()
        self.migrate_db()
        self.before_server_reload()
        self.create_conf()
        self.open_site()
        self.after_deploy()

    def create_conf(self):
        '''Update or create conf for nginx and uwsgi'''
        create_uwsgi_conf()
        create_nginx_conf()

    def sync(self):
        '''Sync data server -> local'''
        sync()

    def before_server_reload(self):
        '''For inheritance class'''
        pass

    def before_deploy(self):
        '''For inheritance class'''
        pass

    def after_deploy(self):
        '''For inheritance class'''
        pass

    def git_load(self):
        with cd(env.APP_ROOT):
            run('git pull')
            run('git checkout %s && git pull' % env.GIT_BRANCH)

    def pip_sync(self):
        text_print('pip-sync')
        with venv_context():
            file_ = 'Pipfile.lock'
            if not equal_new_file_and_bak(file_):
                run('pipenv install --deploy')
            run('cp {file} {file}.bak'.format(file=file_))

    def collectstatic(self):
        text_print('collectstatic')
        self.manage_py('collectstatic --noinput')

    def manage_py(self, cmd):
        with venv_context():
            run(self._manage_py_tpl % cmd)

    def manage_py_local(self, cmd):
        local(self._manage_py_tpl % cmd)

    @property
    def _manage_py_tpl(self):
        return './manage.py %s --settings={env.DJANGO_SETTINGS_FILE}'.format(
            env=env, )

    def migrate_db(self):
        text_print('migrate')
        self.manage_py('migrate')

    def open_site(self):
        with settings(warn_only=True):
            cmd = 'which xdg-open'
            out = local(cmd, capture=True)
            if out.return_code == 0:
                local('xdg-open http://%s' % env.SITE_NAME)

    def install_rsa_pub_key_to_server(self, from_user, from_host, to_user,
                                      to_host):

        cmd = 'scp {user}@{host}:~/.ssh/id_rsa.pub .'.format(
            user=from_user,
            host=from_host,
        )
        local(cmd)
        cmd = 'scp id_rsa.pub {user}@{host}:~'.format(
            user=to_user,
            host=to_host,
        )
        local(cmd)
        local('rm id_rsa.pub')
        cmd = ('ssh {user}@{host} "cat ~/id_rsa.pub >>' +
               ' ~/.ssh/authorized_keys"').format(
                   user=to_user,
                   host=to_host,
               )
        local(cmd)
        cmd = ('ssh {from_user}@{from_host}'
               ' "ssh -o StrictHostKeyChecking=no {to_user}@{to_host} ls"'
               ).format(
                   from_user=from_user,
                   from_host=from_host,
                   to_user=to_user,
                   to_host=to_host,
               )
        local(cmd)

    def create_htpasswd_auth_file(self):
        require.deb.packages(['apache2-utils'])
        cmd = 'htpasswd -bc {file} {user} {password}'.format(
            user=env.user,
            password=env.password,
            file=env.HTPASSWD_FILE,
        )
        sudo(cmd)


def deploy(*args):
    '''Deploy from server. For first deploy call deploy:first'''
    Deploy(*args)


def sync():
    ConfFabficEnvMixin().conf_env()
    text_print('sync data')
    cmd = ('rsync --progress --delete -av ' +
           '{env.user}@{env.host_string}:{env.DATA_ROOT} ' +
           '{local_data}').format(
               env=env,
               local_data=env.DATA_LOCAL_ROOT[:-1],
           )
    local(cmd)
